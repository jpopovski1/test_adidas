import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.firefox.FirefoxDriver;
public class SeleniumTest extends SeleniumObjects {

    String URL = "http://store.demoqa.com";
    String PATH_GECKO_DRIVER = "../test_adidas/src/main/resources/geckodriver.exe";

    @Before
    public void startSetup() {
        //Initialise driver and all needed elements to make sure test can be conducted
        System.setProperty("webdriver.gecko.driver",PATH_GECKO_DRIVER);
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.navigate().to(URL);
    }

    @Test
    public void dryRunTest() {
        scrollToBottomOfPage();
        navigateToSamplePage();
        enterComment();
        enterEmail();
        enterName();
        enterWebsite();
        clickOnPostComment();
        getError();
    }

    @After
    public void killDriver() {
        driver.quit();
    }
}
