import org.junit.Test;
import static org.hamcrest.Matchers.equalTo;

public class ApiSmokeTest extends ApiHelpers {
    String APIKEY = "1c1b369909b99a94e3762a506e86e5ed";
    String CITY_NAME = "Belgrade";
    String COUNTRY_CODE = "RS";

    @Test
    //Test for basic functionality
    public void dryRunTest() {
        callWeatherService(CITY_NAME, APIKEY).getBody();

    }

    @Test
    //Test is proper city shown
    public void assertCityName() {
        callWeatherService(CITY_NAME, APIKEY).then().body("name", equalTo(CITY_NAME));
    }

    @Test
    //Test is proper country shown
    public void assertCountryCode() {
        callWeatherService(CITY_NAME, APIKEY).then().body("sys.country", equalTo(COUNTRY_CODE));
    }

    @Test
    //Test is proper code shown
    public void assertCodeInsideResponse() {
        callWeatherService(CITY_NAME, APIKEY).then().body("cod", equalTo(200));
        //Actually here we have some known issue, since typo for code can be seen.
    }
}
