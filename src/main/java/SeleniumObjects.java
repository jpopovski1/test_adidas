import junit.framework.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;


public class SeleniumObjects {

    protected WebDriver driver;

    //LOCATORS

    By logo = By.className("site-anchor");
    By samplePageLink = By.id("menu-item-54");
    By commentField = By.id("comment");
    By nameField = By.id("author");
    By emailField = By.id("email");
    By webSiteField = By.id("url");
    By postCommentButton = By.id("submit");
    By errorLink = By.id("error-page");


    //METHODS FOR ACTIONS

    //Scroll to the bottom to be able to see "Sample page link"
    public void scrollToBottomOfPage() {
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    //Click on Sample Page link
    public void navigateToSamplePage() {
        driver.findElement(samplePageLink).isDisplayed();
        driver.findElement(samplePageLink).click();
    }

    //Enter text in proper fields
    public void enterComment() {
        driver.findElement(commentField).isDisplayed();
        driver.findElement(commentField).sendKeys("Some basic stuff");
    }

    public void enterName() {
        driver.findElement(nameField).isDisplayed();
        driver.findElement(nameField).sendKeys("Doe");
    }

    public void enterEmail() {
        driver.findElement(emailField).isDisplayed();
        driver.findElement(emailField).sendKeys("mymail.isnot_correct");
    }

    public void enterWebsite() {
        driver.findElement(webSiteField).isDisplayed();
        driver.findElement(webSiteField).sendKeys("www.testandbreakstuff.com");
    }

    //Click on post comment
    public void clickOnPostComment() {
        driver.findElement(postCommentButton).isDisplayed();
        driver.findElement(postCommentButton).click();
    }

    //GetError and validate it
    public void getError() {
        driver.findElement(errorLink).isDisplayed();
        Assert.assertTrue(driver.findElement(errorLink).isDisplayed());
        Assert.assertTrue(driver.findElement(errorLink).getText().toString()
                .equals("ERROR: please enter a valid email address.\n" +
                        "« Back"));
        //I got error as it is. I did not wanted to trim text intentionally.
    }
}
