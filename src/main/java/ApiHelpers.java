import io.restassured.response.Response;

import static io.restassured.RestAssured.get;
import static io.restassured.http.ContentType.JSON;


public class ApiHelpers {

    public Response callWeatherService(String cityName, String apiKey) {
        //This method only calls and makes sure that proper response is ready for testing.
        Response response = get("http://api.openweathermap.org/data/2.5/weather?q=" + cityName + "&APPID=" + apiKey + "").then().assertThat().contentType(JSON).and().extract().response();
        return response;
    }
}
